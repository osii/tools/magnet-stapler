# Magnet Stapler

## Overview

The goal of this tool is to facilitate the mounting of cube magnets (12×12×12mm) into pockets, e.g. for the rings of the [30cm Halbach Magnet](https://gitlab.com/osii/magnet/30cm-halbach-magnet).
It's faster and easier than using your bare hands and your fingers will be happy too :)

![Stapler in use](res/img/stapler-in-use.jpg){width=100%}

The design consists of two parts:

- the [holder](src/holder.FCStd), supplying the magnets to be pushed down while holding the already mounted magnets in place,
- the [stamp](src/stamp.FCStd) to push down the magnets.

Both parts can be 3D printed using a SLA printer.
At least the holder should be printed in a translucent material, so you can spot the pockets in the ring underneath when using it.

## Make

1. Print holder & stamp using a SLA printer and translucent material. STLs are provided in the [release notes](https://gitlab.com/osii/tools/magnet-stapler/-/releases).
	- When removing the nubs of the supports be careful not to damage the surface of the holder too much (e.g. when using a file). At the end you should still be able to see enough through the material to spot the magnet pockets.
	- Printing the holder vertically (duct facing upwards) minimizes the number of supports, but reduces the transparency of the plate (can be fixed by polishing it, but didn't make a significant difference for me anyways).
2. Mount a permanent magnet in the front pocket of the holder; magnet north facing away from the duct.
3. Done! Enjoy your stapler :)

## Usage

1. Insert a string of cube magnets (12×12×12mm) into the duct of the holder. Make sure they have the same orientation as the magnet in the front pocket.
2. Place the holder over a pocket so that the contours of the hole of the holder and the pocket of the ring underneath are aligned.
3. Use the stamp to push the magnet through the hole into the pocket.
4. Pull out the stamp and slide away the holder. The magnet in the front pocket should move up the string of magnets automatically, so you can repeat the procedure from step 2 on for all remaining pockets :)

## Contributors (alphabetical order)

Martin Häuer

## License and Liability

This is an open source hardware project licensed under the CERN Open Hardware Licence Version 2 - Weakly Reciprocal. For more information please check [LICENSE](LICENSE) and [DISCLAIMER](DISCLAIMER.pdf).

